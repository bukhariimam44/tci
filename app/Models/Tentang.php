<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tentang extends Model
{
    // use HasFactory;
    protected $fillable = [
        'gambar','judul', 'konten','admin_update','created_at','updated_at'
    ];
    public function UserId(){
        return $this->belongsTo('App\Models\User','admin_update');
    }
}
