@extends('layouts.umum')
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
            <div class="container">
                <div class="page-title-content">
                    <h2>Daftar</h2>
                    <ul>
                        <li><a href="{{route('index')}}">Beranda</a></li>
                        <li>Daftar</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Page Banner -->

        <!-- Start Register Area -->
        <section class="register-area ptb-50">
            <div class="container">
                <div class="register-form">
                    <h2>Daftar</h2>

                    <form>
                        <div class="form-group">
                            <label>Nama Lengkap</label>
                            <input type="text" class="form-control" name="name" placeholder="Nama Lengkap...">
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" name="email" placeholder="Email...">
                        </div>

                        <div class="form-group">
                            <label>Nomor HP</label>
                            <input type="text" name="nohp" class="form-control" placeholder="Nomor HP...">
                        </div>

                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Password...">
                        </div>

                        <!-- <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="checkme">
                                    <label class="form-check-label" for="checkme">Show password?</label>
                                </div>
                            </div>
                        </div> -->

                        <button type="submit">Daftar Sekarang</button>
                    </form>

                    <div class="important-text">
                        <p>Sudah punya akun? <a href="{{route('login')}}">Login Sekarang!</a></p>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Register Area -->
@endsection