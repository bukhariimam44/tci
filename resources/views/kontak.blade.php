@extends('layouts.umum')

@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
            <div class="container">
                <div class="page-title-content">
                    <h2>Kontak</h2>
                    <ul>
                        <li><a href="{{route('index')}}">Beranda</a></li>
                        <li>Kontak</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Page Banner -->

        <!-- Start Contact Area -->
        <section class="contact-area ptb-50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="contact-map">
                            {!! $datas->maps !!}
                            <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d27982.0992479593!2d-81.35428553933833!3d28.75650994456714!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88e76d5129fed6b1%3A0x1a6cd960f325cfcb!2sLake%20Mary%2C%20FL%2032746%2C%20USA!5e0!3m2!1sen!2sbd!4v1602575597158!5m2!1sen!2sbd"></iframe> -->
                        </div>

                        <ul class="contact-info">
                            <li>
                                <span>Alamat :</span> <br>
                                {{ $datas->alamat }}
                            </li>
                            <li>
                                <span>Telpon :</span>
                                <a href="tel:{{ $datas->telpon }}">{{ $datas->telpon }}</a>
                            </li>
                            <li>
                                <span>Email :</span>
                                <a href="mailto:{{ $datas->email }}">{{ $datas->email }}</a>
                            </li>
                        </ul>

                        <div class="contact-form">
                            <div class="title">
                                <h3>Siap memulai?</h3>
                                <p>Kirimkan Pesan Kepada Kami</p>
                            </div>

                            <form id="contactForm">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="name" class="form-control" id="name" required data-error="Masukan nama lengkap" placeholder="Nama Lengkap">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <input type="email" name="email" class="form-control" id="email" required data-error="Masukan Email" placeholder="Email">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="phone_number" class="form-control" id="phone_number" required data-error="Masukan Nomor HP" placeholder="Nomor HP">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="subject" class="form-control" id="subject" required data-error="Masukan Judul Pesan" placeholder="Judul Pesan">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12">
                                        <div class="form-group">
                                            <textarea name="message" id="message" class="form-control" cols="30" rows="6" required data-error="Masukan Isi Pesan" placeholder="Ketik pesan disini..."></textarea>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" data-error="Setujui Syarat dan Ketentuan." name="setujui" required id="setujui">
                                            <label class="form-check-label" for="checkme">
                                                Setujui <a href="#">Syarat</a> and <a href="#">Ketentuan </a>pengiriman pesan. <br>
                                                <div class="help-block with-errors"></div>
                                            </label>
                                            
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12">
                                        <button type="submit" class="default-btn">Kirim Pesan</button>
                                        <div id="msgSubmit" class="h3 text-center hidden"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <aside class="widget-area">
                            <section class="widget widget_stay_connected">
                                <h3 class="widget-title">Tetap terhubung</h3>
                                
                                <ul class="stay-connected-list">
                                    <li>
                                        <a href="#">
                                            <i class='bx bxl-facebook'></i>
                                            120,345 Fans
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#" class="twitter">
                                            <i class='bx bxl-twitter'></i>
                                            25,321 Followers
                                        </a>
                                    </li>

                                    <!-- <li>
                                        <a href="#" class="linkedin">
                                            <i class='bx bxl-linkedin'></i>
                                            7,519 Connect
                                        </a>
                                    </li> -->

                                    <li>
                                        <a href="#" class="youtube">
                                            <i class='bx bxl-youtube'></i>
                                            101,545 Subscribers
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#" class="instagram">
                                            <i class='bx bxl-instagram'></i>
                                            10,129 Followers
                                        </a>
                                    </li>

                                    <!-- <li>
                                        <a href="#" class="wifi">
                                            <i class='bx bx-wifi'></i>
                                            952 Subscribers
                                        </a>
                                    </li> -->
                                </ul>
                            </section>

                            <section class="widget widget_newsletter">
                                <div class="newsletter-content">
                                    <h3>Berlangganan</h3>
                                    <p>Berlangganan untuk mendapat berita terbaru dan terupdate.</p>
                                </div>   

                                <form class="newsletter-form" data-toggle="validator">
                                    <input type="email" class="input-newsletter" placeholder="Masukan Email Kamu" name="email" required autocomplete="off">
            
                                    <button type="submit">Berlangganan</button>
                                    <div id="validator-newsletter" class="form-result"></div>
                                </form>
                            </section>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Contact Area -->
        @endsection